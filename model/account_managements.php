<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Model;

class AccountManagements extends Model {

    protected $table = "account_managements";
    protected $primaryKey = "id";
    public $timestamps = false;
    public $incrementing = false;

}
