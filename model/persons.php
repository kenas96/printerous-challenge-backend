<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Model;

class Persons extends Model {

    protected $table = "persons";
    protected $primaryKey = "id";
    public $timestamps = false;
    public $incrementing = false;

}
