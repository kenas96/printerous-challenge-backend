<?php

namespace Model;

use Illuminate\Database\Eloquent\Model as Model;

class Organizations extends Model {

    protected $table = "organizations";
    protected $primaryKey = "id";
    public $timestamps = false;
    public $incrementing = false;

    public function persons() {
        return $this->hasMany("Model\Persons", "organization_id");
    }

}
