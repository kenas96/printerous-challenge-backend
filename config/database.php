<?php

$container = $app->getContainer();

$manager = new \Illuminate\Database\Capsule\Manager;
$manager->addConnection($container["settings"]["db"]);
$manager->setAsGlobal();
$manager->BootEloquent();