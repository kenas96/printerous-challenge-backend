<?php

require 'config.php';

return[
    "settings" => [
        "displayErrorDetails" => TRUE,
        "db" => [
            "driver" => DB_DRIVER,
            "host" => DB_HOST,
            "username" => DB_USER,
            "password" => DB_PASSWORD,
            "database" => DB_NAME,
            "charset" => CHARSET,
            "collation" => COLLATION,
            "prefix" => PREFIX
        ]
    ]
];
