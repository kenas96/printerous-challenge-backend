<?php

require_once __DIR__ . '/config.php';

use Slim\Http\Response as Response;
use Slim\Middleware\JwtAuthentication as JwtAuthentication;
use Helper\JwtHelper as JwtHelper;
use Helper\JsonHelper as JsonHelper;

//CORS
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Credentials', true)
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, auth-user')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->add(new JwtAuthentication([
    "path" => "/",
    "secure" => false,
    "passthrough" => ["/api/login"],
    "secret" => SECRET_KEY,
    "error" => function ($req, Response $res, $args) {
        $message = ($args["message"] == "Expired token") ? "Token Expired" : $args["message"];
        $statusCode = ($args["message"] == "Expired token") ? CODE_ERR_TOKEN_EXP : CODE_ERR_TOKEN_WRONG;
        return $res->withJson(JsonHelper::errorResponse($message, $statusCode), 422);
    }
]));
