<?php

define("DB_DRIVER", "mysql");
define("DB_HOST", "localhost");
define("DB_NAME", "printerous");
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("CHARSET", "utf8");
define("COLLATION", "utf8_unicode_ci");
define("PREFIX", "");
define("SECRET_KEY", "P@ssw0rdTOk3n");

define("CODE_SUCC", 1005);
define("CODE_ERR", 1000);
define("CODE_ERR_TOKEN_WRONG", 1001);
define("CODE_ERR_TOKEN_EXP", 1002);
define("CODE_ERR_VERSION", 1003);
define("CODE_ERR_NULL", 1004);

