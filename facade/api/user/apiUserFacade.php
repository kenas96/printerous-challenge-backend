<?php

namespace ApiFacade;

use slim\Http\Request as Request;
use slim\Http\Response as Response;
use Model\AccountManagements as AccountManagements;
use Helper\JwtHelper as JwtHelper;
use Helper\JsonHelper as JsonHelper;

class ApiUserFacade {

    public function Login(Request $req, Response $res, $args) {
        try {
            $data = $req->getParsedBody();

            $user = AccountManagements::where('email', $data["email"])
                    ->first(["id", "email", "password"]);

            if ($user) {
                if (password_verify($data["password"], $user->password)) {
                    $token_data = array(
                        "id" => $user->id
                    );

                    $resData = [
                        "token" => JwtHelper::createToken($token_data),
                        "user" => $user
                    ];
                    return $res->withJson(JsonHelper::successResponse($resData));
                } else {
                    return $res->withJson(JsonHelper::errorResponse('Incorrect password', CODE_ERR), 422);
                }
            } else {
                return $res->withJson(JsonHelper::errorResponse('Email not found', CODE_ERR_NULL), 422);
            }
        } catch (\Exception $ex) {
            return $res->withJson(JsonHelper::errorResponse($ex->getMessage(), CODE_ERR), 422);
        }
    }

}
