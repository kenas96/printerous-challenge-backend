<?php

namespace ApiFacade;

use slim\Http\Request as Request;
use slim\Http\Response as Response;
use Model\Organizations as Organizations;
use Model\Persons as Persons;
use Helper\JsonHelper as JsonHelper;
use Helper\StringHelper as StringHelper;

class ApiOrganizationFacade {

    public function __construct() {
        
    }

    public function selectPage(Request $req, Response $res, $args) {
        try {
            $data = $req->getParsedBody();
            extract($data);
            extract($order);

            $list = Organizations::select(["*"])
                    ->with(["persons" => function($list) {
                    
                }]);

            if (is_array($criteria) && count($criteria)) {
                foreach ($criteria as $cri) {
                    $val = $cri["value"];
                    $list->where($cri["criteria"], "like", "%$val%");
                }
            }
            $count = $list->count();
            $pageCount = ceil($count / $pageSize);
            $list = $list->orderBy($column, $direction)
                    ->take($pageSize)
                    ->offset($pageSize * ($page - 1));

            $resData = [
                "rows" => $list->get(),
                "rowCount" => $count,
                "pageCount" => $pageCount
            ];
            return $res->withJson(JsonHelper::successResponse($resData));
        } catch (\exception $ex) {
            return $res->withJson(JsonHelper::errorResponse($ex->getMessage(), CODE_ERR));
        }
    }

    public function insert(Request $req, Response $res, $args) {
        try {
            $data = $req->getParsedBody();

            $organization = new Organizations();
            foreach ($data as $key => $val) {
                $organization->$key = $val;
            }

            if (isset($data["logoName"])) {
                $imageRaw = $data["logo"];
                $arrImageRaw = explode(",", $imageRaw);
                $imageName = 'organization-logo-' . time() . '-' . $data["logoName"];
                $ImageBase64 = $arrImageRaw[1];
                $imageDecoded = base64_decode($ImageBase64);
                file_put_contents(__DIR__ . "/../../../uploads/organization_logo/$imageName", $imageDecoded);
                $organization->logo = $imageName;
                unset($organization->logoName);
            }
            $organization->save();

            $resData = [];
            return $res->withJson(JsonHelper::successResponse($resData));
        } catch (\exception $ex) {
            return $res->withJson(JsonHelper::errorResponse($ex->getMessage(), CODE_ERR));
        }
    }

    public function update(Request $req, Response $res, $args) {
        try {
            $data = $req->getParsedBody();

            $organization = Organizations::find($data["id"]);
            if (is_null($organization)) {
                return $res->withJson(JsonHelper::errorResponse('Data not found', CODE_ERR_NULL));
            } else {
                if (isset($data["logoName"])) {
                    $old_image = $organization["logo"];
                    $new_image = 'organization-logo-' . time() . '-' . $data["logoName"];
                }

                if (isset($data["logoName"])) {
                    $imageRaw = $data["logo"];
                    $arrImageRaw = explode(",", $imageRaw);
                    $ImageBase64 = $arrImageRaw[1];
                    $imageDecoded = base64_decode($ImageBase64);
                    file_put_contents(__DIR__ . "/../../../uploads/organization_logo/$new_image", $imageDecoded);
                    unlink(__DIR__ . "/../../../uploads/organization_logo/$old_image");
                    $organization->logo = $new_image;
                    unset($organization->logoName);
                }
                $organization->save();

                $resData = [];
                return $res->withJson(JsonHelper::successResponse($resData));
            }
        } catch (\exception $ex) {
            return $res->withJson(JsonHelper::errorResponse($ex->getMessage(), CODE_ERR));
        }
    }

    public function delete(Request $req, Response $res, $args) {
        try {
            $id = $args["id"];
            $organization = Organizations::find($id);
            if (is_null($organization)) {
                return $res->withJson(JsonHelper::errorResponse('Data not found', CODE_ERR_NULL));
            } else {
                $old_image = $organization["logo"];
                unlink(__DIR__ . "/../../../uploads/organization_logo/$old_image");
                $organization->delete();

                $resData = [];
                return $res->withJson(JsonHelper::successResponse($resData));
            }
        } catch (\exception $ex) {
            return $res->withJson(JsonHelper::errorResponse($ex->getMessage(), CODE_ERR));
        }
    }

}
