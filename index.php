<?php

use slim\Http\Request as Request;
use slim\Http\Response as Response;

require './vendor/autoload.php';

$settings = require __DIR__ . '/config/settings.php';
$app = new \Slim\App($settings);

require __DIR__ . '/config/database.php';
require __DIR__ . '/config/middleware.php';

$app->group("/api", function () {

    $this->post('/login', "ApiFacade\ApiUserFacade:Login");

    $this->group("/organizations", function () {
        $this->post("/paging", "ApiFacade\ApiOrganizationFacade:selectPage");
        $this->post("[/]", "ApiFacade\ApiOrganizationFacade:insert");
        $this->put("[/]", "ApiFacade\ApiOrganizationFacade:update");
        $this->delete("/{id}", "ApiFacade\ApiOrganizationFacade:delete");
    });

    $this->group("/persons", function () {
        $this->post("/paging", "ApiFacade\ApiPersonFacade:selectPage");
        $this->post("[/]", "ApiFacade\ApiPersonFacade:insert");
        $this->put("[/]", "ApiFacade\ApiPersonFacade:update");
        $this->delete("/{id}", "ApiFacade\ApiPersonFacade:delete");
    });
});

$app->run();
