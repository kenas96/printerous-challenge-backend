<?php

namespace Helper;

class ImageHelper {

    function file_copy_to_folder($source_file, $destination_folder, $filename) {
        $arrext = explode('.', $source_file['name']);
        $jml = count($arrext) - 1;
        $ext = $arrext[$jml];
        $ext = strtolower($ext);
        $ret['ext'] = $ext;
        if (!is_dir($destination_folder)) {
            mkdir($destination_folder, 0755);
        }
        $destination_folder .= $filename . '.' . $ext;

        if (@move_uploaded_file($source_file['tmp_name'], $destination_folder)) {
            $ret = $filename . "." . $ext;
        }
        return $ret;
    }


    function copy_image_resize_to_folder($source_file, $destination_folder, $filename, $max_width, $max_height) {
        $image_info = getimagesize($source_file);
        $source_pic_width = $image_info[0];
        $source_pic_height = $image_info[1];

        $x_ratio = $max_width / $source_pic_width;
        $y_ratio = $max_height / $source_pic_height;

        if (($source_pic_width <= $max_width) && ($source_pic_height <= $max_height)) {
            $tn_width = $source_pic_width;
            $tn_height = $source_pic_height;
        } elseif (($x_ratio * $source_pic_height) < $max_height) {
            $tn_height = ceil($x_ratio * $source_pic_height);
            $tn_width = $max_width;
        } else {
            $tn_width = ceil($y_ratio * $source_pic_width);
            $tn_height = $max_height;
        }

        if (!is_dir($destination_folder)) {
            mkdir($destination_folder, 0755);
        }

        switch ($image_info['mime']) {
            case 'image/gif':
                if (imagetypes() & IMG_GIF) {
                    $src = imageCreateFromGIF($source_file);
                    $destination_folder .= "$filename";
                    $namafile = "$filename";
                }
                break;

            case 'image/jpeg':
                if (imagetypes() & IMG_JPG) {
                    $src = imageCreateFromJPEG($source_file);
                    $destination_folder .= "$filename";
                    $namafile = "$filename";
                }
                break;

            case 'image/pjpeg':
                if (imagetypes() & IMG_JPG) {
                    $src = imageCreateFromJPEG($source_file);
                    $destination_folder .= "$filename";
                    $namafile = "$filename";
                }
                break;

            case 'image/png':
                if (imagetypes() & IMG_PNG) {
                    $src = imageCreateFromPNG($source_file);
                    $destination_folder .= "$filename";
                    $namafile = "$filename";
                }
                break;

            case 'image/wbmp':
                if (imagetypes() & IMG_WBMP) {
                    $src = imageCreateFromWBMP($source_file);
                    $destination_folder .= "$filename.bmp";
                    $namafile = "$filename.bmp";
                }
                break;
        }

        $tmp = imagecreatetruecolor($tn_width, $tn_height);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $tn_width, $tn_height, $source_pic_width, $source_pic_height);

        //**** 100 is the quality settings, values range from 0-100.
        switch ($image_info['mime']) {
            case 'image/jpeg':
                imagejpeg($tmp, $destination_folder, 50);
                break;

            case 'image/gif':
                imagegif($tmp, $destination_folder, 50);
                break;

            case 'image/png':
                imagepng($tmp, $destination_folder);
                break;

            default:
                imagejpeg($tmp, $destination_folder, 50);
                break;
        }

        return ($namafile);
    }

    function getBytesFromHexString($hexdata) {
        for ($count = 0; $count < strlen($hexdata); $count += 2)
            $bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

        return implode($bytes);
    }

    function getImageMimeType($imagedata) {
        $imagemimetypes = array(
            "jpeg" => "FFD8",
            "png" => "89504E470D0A1A0A",
            "gif" => "474946",
            "bmp" => "424D",
            "tiff" => "4949",
            "tiff" => "4D4D"
        );

        foreach ($imagemimetypes as $mime => $hexbytes) {
            $bytes = $this->getBytesFromHexString($hexbytes);
            if (substr($imagedata, 0, strlen($bytes)) == $bytes)
                return $mime;
        }

        return NULL;
    }

}
