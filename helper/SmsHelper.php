<?php

namespace Helper;

require_once './config/config.php';

use Twilio\Rest\Client;

class SmsHelper {

    public function sendSms($post) {
        $sid = TWILIO_SMS_ACCOUNT_ID;
        $token = TWILIO_SMS_AUTH_TOKEN;
        $client = new Client($sid, $token);

        $people = array(
            $post["mobile_number"] => $post["name"]
        );

        $from_body = array(
            'from' => TWILIO_SMS_FROM_NUMBER,
            'body' => $post["body"]
        );

        $errorIds = array();

        try {
            $client->messages->create($post["mobile_number"], $from_body);
            $message = "Send Success";
            $status = true;
        } catch (\Twilio\Exceptions\RestException $e) {
            array_push($errorIds, $post["name"]);
            $message = $e->getMessage();
            $status = false;
        }

        return array("sms_status" => $status, "sms_message" => $message);
    }

}
