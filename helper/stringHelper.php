<?php

namespace Helper;

require_once './config/config.php';

class StringHelper {

    function hashing($string) {
        $hashedString = password_hash($string, PASSWORD_BCRYPT, array('cost' => 9));
        return $hashedString;
    }

    function randomCode($loop) {
        $chars = 'abcdefghijklmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $max = strlen($chars) - 1;
        $codeTemp = null;
        for ($i = 0; $i < $loop; $i++) {
            $codeTemp .= $chars{mt_rand(0, $max)};
        }
        $code = $codeTemp;
        return $code;
    }

}
