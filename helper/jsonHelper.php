<?php

namespace Helper;

class JsonHelper {

    public function successResponse($data) {
        return [
            "meta" => [
                "status" => true,
                //"http_code" => 200,
                "message" => "Success",
                "status_code" => CODE_SUCC
            ],
            "data" => $data
        ];
    }

    public function errorResponse($message, $status_code) {
        return [
            "meta" => [
                "status" => false,
                //"http_code" => 422,
                "message" => $message,
                "status_code" => $status_code
            ],
            "data" => []
        ];
    }

}
