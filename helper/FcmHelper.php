<?php

namespace Helper;

class FcmHelper {

    function __construct() {
        
    }

    public function sendAndroid($to, $message) {
        $fields = array(
            'data' => $message,
            'to' => $to,
        );
        return $this->sendPushNotification($fields);
    }

    public function sendIos($to, $message) {
        $fields = array(
            'notification' => $message,
            'to' => $to,
        );
        return $this->sendPushNotification($fields);
    }

    private function sendPushNotification($fields) {


        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }

}
