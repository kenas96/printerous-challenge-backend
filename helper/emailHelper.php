<?php

namespace Helper;

require_once './config/config.php';
require_once('PHPMailer/PHPMailerAutoload.php');

class EmailHelper {

    public function sendEmail($post) {
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = SMTP_GMAIL_HOST;
        $mail->Port = SMTP_GMAIL_PORT;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = SMTP_GMAIL_USER;
        $mail->Password = SMTP_GMAIL_PASSWORD;
        $mail->setFrom('taco.comm.app@gmail.com', 'Taco');
        $mail->addReplyTo('taco.comm.app@gmail.com', 'Taco');
        $mail->addAddress($post["email"], $post["name"]);

        $mail->isHTML(true);
        $mail->Subject = $post["subject"];
        $mail->Body = $post["body"];
        if (!$mail->send()) {
            return array("email_status" => false, "email_message" => $mail->ErrorInfo);
        } else {
            return array("email_status" => true, "email_message" => "");
        }
    }

}
