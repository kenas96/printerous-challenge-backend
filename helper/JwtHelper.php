<?php

namespace Helper;

use Firebase\JWT\JWT as JWT;

//use Model\SystemParameter as SystemParameter;

class JwtHelper {

    public function decodeToken($headerAuth) {
        $token = str_replace("Bearer ", "", $headerAuth[0]);
        $tokenDecoded = JWT::decode($token, SECRET_KEY, array('HS256'));
        return $tokenDecoded;
    }

    public function createToken($data = array()) {
        $payload = [
            "iss" => "CRM App",
            "aud" => "CRM App",
            "iat" => (int) microtime(true),
            "exp" => (int) microtime(true) + 315360000,
            "data" => $data
        ];

        $token = JWT::encode($payload, SECRET_KEY);
        return $token;
    }

}
