<?php

namespace Helper;

class UrlHelper {
    public function getBaseUrl($req){
        return $_SERVER['HTTP_HOST'].$req->getUri()->getBasePath();
    }
}
