<?php

namespace Helper;

use Slim\Http\Request as Request;
use Slim\Http\Response as Response;
use Illuminate\Database\Capsule\Manager as Manager;

class UserHelper {

    public function ForgotPasswordTokenCheck($token) {

        $query = Manager::connection()->getPdo()
                ->prepare("SELECT * FROM tbl_user WHERE password_recovery_token = ? AND password_recovery_expired >= UNIX_TIMESTAMP() LIMIT 1");
        $query->execute([$token]);
        $result = $query->fetchAll(\PDO::FETCH_OBJ);
        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function getUserDataFromForgotPasswordToken($token) {
        $return = array();
        $list = Manager::table("tbl_user");
        $list->where("tbl_user.password_recovery_token", "=", $token);
        $rows = $list->get();
        if (count($rows) > 0) {
            $return = $rows[0];
        }

        return $return;
    }

}
